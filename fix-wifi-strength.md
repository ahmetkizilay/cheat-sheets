### Fix wifi strength

Running fedora on my laptop with rtl8723be wireless network card, the signal strength was too weak.
To fix:

```
 modinfo -p rtl8723be
 sudo modprobe -r rtl8723be
 sudo modprobe rtl8723be ant_sel=1
 iwlist scan | egrep -i 'ssid|level'
```
Reference: https://ask.fedoraproject.org/en/question/99831/fedora-25-wifi-doesnot-connect/
